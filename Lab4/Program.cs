﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace Lab4 {
    internal static class Program {
        private const string HomePath = @"C:/users/dmitry/Spolab4/";
        private static string[] _arguments;
        private static bool _isForced;
        private static bool _isInteractive;
        private static bool _isVerb;
        private static ConsoleKeyInfo _keyPressed;
        private static List<string> _paths;
        private static void Main(string[] args) {
            do {
                Console.Write("dmitry@:~$");
                StartHandle(Console.ReadLine());
                _keyPressed = Console.ReadKey();
            } while (_keyPressed.KeyChar != 'Q');
        }
        private static void StartHandle(string command) {
            _paths = new List<string>();
            _isForced = false;
            _isInteractive = false;
            _isVerb = false;
            _arguments = command.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            GetPaths(command);
            if (_paths.Count == 0) {
                Console.WriteLine("rm: пропущен операнд");
            }
            if (_arguments.Length > 0 && _arguments[0] == "rm") {
                if (_arguments.Contains("-f")) {
                    SetForce();
                }
                if (_arguments.Contains("-i")) {
                    SetInteractive();
                }
                if (_arguments.Contains("-v")) {
                    SetVerbose();
                }
                if (!_arguments.Contains("-r")) {
                    foreach (var file in _paths) {
                        var path = PathAnalyze(HomePath, file);
                        if (File.Exists(path)) {
                            if (_isInteractive) {
                                Console.WriteLine("rm: удалить файл " + file + "?");
                                var answ = Console.ReadLine()?.ToLower();
                                if (answ == "yes" || answ == "y") {
                                    File.Delete(path);
                                    if (_isVerb) {
                                        Console.WriteLine("rm: удалён " + file);
                                    }
                                    break;
                                } else if (answ == "no" || answ == "n") {
                                    continue;
                                } else {
                                    return;
                                }
                            } else {
                                File.Delete(path);
                                if (_isVerb) {
                                    Console.WriteLine("rm: удален " + file);
                                }
                            }
                        } else if (Directory.Exists(path)) {
                            if (!_isForced) {
                                Console.WriteLine("rm: невозможно удалить " + file + " : это каталог");
                            }
                        } else {
                            if (!_isForced) {
                                Console.WriteLine("rm: невозможно удалить " + file +
                                                  ": нет такого файла или каталога");
                            }
                        }
                    }
                } else {
                    foreach (var fileOrDir in _paths) {
                        var path = PathAnalyze(HomePath, fileOrDir);
                        if (Directory.Exists(path)) {
                            var info = new DirectoryInfo(path);
                            if (info.GetFiles().Length == 0 || info.GetDirectories().Length == 0) {
                                if (_isInteractive) {
                                    Console.WriteLine($"rm: удалить каталог {info.Name} ?");
                                    var answ = Console.ReadLine()?.ToLower();
                                    if (answ == "y" || answ == "yes") {
                                        Directory.Delete(path);
                                        if (_isVerb) {
                                            Console.WriteLine($"rm: удален каталог {info.Name}");
                                        }
                                    }
                                } else {
                                    Directory.Delete(path);
                                }
                               
                            } else {
                                if (_isInteractive) {
                                    Console.WriteLine($"rm: Спуститься в каталог {fileOrDir} ?");
                                    var answ = Console.ReadLine()?.ToLower();
                                    if (answ == "y" || answ == "yes") {
                                        RecursiveDeleteDirectory(path, Directory.GetParent(path).FullName);
                                    }
                                } else {
                                    RecursiveDeleteDirectory(path, Directory.GetParent(path).FullName);
                                }
                            }
                        } else if (File.Exists(path)) {
                            if (_isInteractive) {
                                Console.WriteLine("rm: удалить файл " + fileOrDir + "?");
                                var answ = Console.ReadLine()?.ToLower();
                                if (answ == "yes" || answ == "y") {
                                    File.Delete(path);
                                    if (_isVerb) {
                                        Console.WriteLine("rm: удален " + fileOrDir);
                                    } else if (answ == "no" || answ == "n") {
                                        continue;
                                    } else {
                                        return;
                                    }
                                }
                            } else {
                                File.Delete(path);
                                if (_isVerb) {
                                    Console.WriteLine("rm: удален " + fileOrDir);
                                }
                            }
                        } else {
                            if (!_isForced) {
                                Console.WriteLine($"rm: невозможно удалить {fileOrDir} нет такого файла или каталога");
                            }
                        }
                    }
                }
            } else {
                if (_arguments.Length == 0) {
                    Console.WriteLine($"{_arguments[0]}: Команда не найдена");
                }
            }
        }
        private static void SetForce() {
            _isForced = true;
            _isInteractive = false;
        }
        private static void SetInteractive() {
            _isInteractive = true;
            _isForced = false;
        }
        private static void SetVerbose() {
            _isVerb = true;
        }
        private static void GetPaths(string arguments) {
            var parts = arguments.Split(' ');
            foreach (var part in parts) {
                if (part != "rm" && part != "-r" && part != "-f" && part != "-v" && part != "-i") {
                    _paths.Add(part);
                }
            }
        }
        private static string PathAnalyze(string homePath, string path) {
            if (string.IsNullOrEmpty(path)) {
                return homePath;
            }
            if (path.StartsWith(@"C:/") || path.StartsWith(@"c:/")) {
                return path;
            }
            if (!path.Contains("/")) {
                return HomePath + path;
            }
            if (!path.StartsWith("../"))
                return HomePath + path;
            var parentPath = homePath;
            while (path.StartsWith("../")) {
                parentPath = Directory.GetParent(parentPath).FullName;
                path = path.Remove(0, 3);
            }
            return parentPath + path;
        }
        private static void RecursiveDeleteDirectory(string path, string parentPath) {
            if (path == parentPath) {
                return;
            }
            var directory = new DirectoryInfo(path);
            if (directory.GetDirectories().Length == 0 && directory.GetFiles().Length == 0) {
                var parent = Directory.GetParent(path).FullName;
                if (_isInteractive) {
                    Console.WriteLine("rm: удалить каталог " + path + " ?");
                    var answ = Console.ReadLine().ToLower();
                    if (answ == "yes" || answ == "y") {
                        Directory.Delete(path);
                        if (_isVerb) {
                            Console.WriteLine($"rm: удален каталог {path}");
                        }
                        return;
                    } else {
                        return;
                    }
                } else {
                    Directory.Delete(path);
                    if (_isVerb) {
                        Console.WriteLine($"rm: удален каталог {path}");
                        return;
                    }
                }
            }
            var subdirs = directory.GetDirectories();
            for (var i = 0; i < subdirs.Length; ++i) {
                if (_isInteractive) {
                    Console.WriteLine("rm: спуститься в каталог " + subdirs[i].FullName + " ?");
                    var answ = Console.ReadLine().ToLower();
                    if (answ == "yes" || answ == "y") {
                        RecursiveDeleteDirectory(subdirs[i].FullName, parentPath);
                    } else {
                        return;
                    }
                } else {
                    RecursiveDeleteDirectory(subdirs[i].FullName, parentPath);
                }
            }
            var files = directory.GetFiles();
            for (int j = 0; j < files.Length; ++j) {
                if (_isInteractive) {
                    Console.WriteLine("rm: удалить файл " + files[j].FullName + " ?");
                    var answ = Console.ReadLine()?.ToLower();
                    if (answ == "yes" || answ == "y") {
                        File.Delete(files[j].FullName);
                        if (_isVerb) {
                            Console.WriteLine($"rm: удалён {files[j].FullName}");
                        }
                    }
                } else {
                    File.Delete(files[j].FullName);
                    if (_isVerb) {
                        Console.WriteLine($"rm: удалён {files[j].FullName}");
                    }
                }
            }
            if(directory.GetFiles().Length == 0 && directory.GetDirectories().Length == 0) {
                if (_isInteractive) {
                    Console.WriteLine($"rm: удалить каталог {path}?");
                    var answ = Console.ReadLine().ToLower();
                    if (answ == "yes" || answ == "y") {
                        Directory.Delete(path);
                        if (_isVerb) {
                            Console.WriteLine($"rm: удален каталог {path}");
                        }
                    } else {
                        return;
                    }
                } else {
                    Directory.Delete(path);
                    if (_isVerb) {
                        Console.WriteLine($"rm: удален каталог {path}");
                    }
                }
            }
        }
    }
}