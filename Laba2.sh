#!/bin/bash

function installPackages() {
	for pkg in "$@"; do
		installed=`dpkg -l |grep $pkg`
		if [ "$installed" != "" ]; then
			echo "Пакет $pkg уже установлен"
		else
			sudo apt-get install "$pkg" -y > /dev/null
			echo "Пакет $pkg Установлен"
	    fi
    done
}

function backupPhpConfig() {
	if [ ! -f /etc/php/7.0/fpm/php.ini ]; then
		echo 'Файл php.ini не найден'
		exit 1
	fi
	if [ ! -f /etc/php/7.0/fpm/php.ini.bak ]; then
		sudo cp /etc/php/7.0/fpm/php.ini /etc/php/7.0/fpm/php.ini.bak
		echo 'Создана резервная копия файла php.ini'
	else
		echo 'Уже есть резервная копия php.ini. Хотите пересоздать?(y/n)'
		read ans
		if [ !$"ans" != "n" ]; then
			sudo cp /etc/php/7.0/fpm/php.ini /etc/php/7.0/fpm/php.ini.bak
			echo 'Резервная копия php.ini изменена'
		fi
	fi
}

function backupNginxConfig (){
	for file in "$@"; do
		if [ ! -f "$file" ]; then
			echo "Не найден файл $file Создать его?(y/n)"
			read crcfg
			if [ ! "$crcfg" != 'n' ]; then
				sudo touch $file
				if [ "$file" -eq "/etc/nginx/sites-enabled/default" ]; then 
					sudo mv "$file" "$file.conf"
				fi

				case "$file" in
					"/etc/nginx/nginx.conf")
						restoreNginxConf
						;;
					"/etc/nginx/sites-enabled/default.conf")
						restoreNginxDefaultConf
						;;
					*)
						;;
				esac
			else
				exit 1
			fi
		else
			if [ ! -f  "$file.bak" ]; then
				sudo cp "$file" "$file.bak"
				echo "Создана резервная копия файла $file"
			else
				echo "Уже есть резервная копия файла $file Хотите пересоздать?(y/n)"
				read crtngbk
				if [ ! $"crtngbk" != "n" ]; then
					sudo cp "$file" "$file"
					echo 'Резервная копия nginx.conf изменена'
				fi
			fi
		fi
	done
}

function changePhpConfig() {
	sudo sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php/7.0/fpm/php.ini
	echo 'Содержание файла php.ini изменено'
}

function changeNginxConfig () {
	sudo sed -i 's:/var/log/nginx/access.log;:'$logDir'/access.log;:' /etc/nginx/nginx.conf
	sudo sed -i 's:/var/log/nginx/error.log;:'$logDir'/error.log;:' /etc/nginx/nginx.conf
	sudo sed -i 's:include /etc/nginx/sites-enabled/\*:include /etc/nginx/sites-enabled/*.conf:' /etc/nginx/nginx.conf
	echo 'nginx.conf изменен'
	sudo sed -i 's:index index.html index.htm index.nginx-debian.html;:index index.php index.html index.htm index.nginx-debian.html;:' /etc/nginx/sites-enabled/default.conf
	sudo sed -i 's:root /var/www/html;:root '$sitesDir';:' /etc/nginx/sites-enabled/default.conf
	sudo sed -i 's:server_name _;:server_name localhost;:' /etc/nginx/sites-enabled/default.conf
	sudo sed -i '56,58 {s/#//}; 60,60{s/#//}; 63,63{s/#//}' /etc/nginx/sites-enabled/default.conf
	echo 'nginx default site conf изменен'
}

function createDirectories () {
	for dir in "$@"; do
		if [ ! -d $dir ]; then
			sudo mkdir "$dir"
			sudo chmod -R 777 "$dir"
			echo "Создана папка $dir"
		else 
			echo "Папка $dir уже существует"
		fi
	done
}

function createLogFiles(){ 
	for file in "$@"; do
		if [ ! -f "$logDir/$file" ]; then
			sudo touch "$logDir/$file"
			sudo chmod 777 "$logDir/$file"
			echo "Создан файл $file"
		else 
			echo "Файл $file уже существует"
		fi
	done
}

function restoreNginxConf() {
	sudo ech0 'user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
	worker_connections 768;
}
http {
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;
	include /etc/nginx/mime.types;
	default_type application/octet-stream;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;
	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;
	gzip on;
	gzip_disable "msie6";
	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*.conf;
}' > /etc/nginx/nginx.conf
}

function restoreNginxDefaultConf(){
	sudo echo 'server {
	listen 80 default_server;
	listen [::]:80 default_server
	root /var/www/html;
	index index.html index.htm index.nginx-debian.html;
	server_name _;
	location / {
		try_files $uri $uri/ =404;
	}
}' > /etc/nginx/sites-enabled/default
}

u=$USER
installPackages nginx php-fpm
backupPhpConfig
changePhpConfig
sitesDir="/home/$u/Sites"
logDir="/home/$u/Logs"
createDirectories "$sitesDir" "$logDir"
createLogFiles access.log error.log
if [ -f /etc/nginx/sites-enabled/default ];  then
	sudo mv /etc/nginx/sites-enabled/default /etc/nginx/sites-enabled/default.conf
fi
backupNginxConfig /etc/nginx/nginx.conf /etc/nginx/sites-enabled/default.conf
changeNginxConfig
read -p 'Создать файл index.php в папке с сайтом и открыть его?(y/n)' crtphpfile
case $crtphpfile in
	[Yy]* )
		sudo touch "$sitesDir/index.php"
		sudo chmod 777 "$sitesDir/index.php"
		sudo echo '<html>
    <body>
    <h1>Lab work complete (probably)</h1>
    <?php
        phpinfo();
    ?>
    </body>
</html>' > "$sitesDir/index.php"
	echo 'Файл создан'
		;;
	[Nn]* )
		echo 'Работа скрипта завершена'
		;;
	* )
		;;
esac
exit 0