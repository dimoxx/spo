﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Lab5 {
    public partial class Form1 : Form {
        private static Random Rnd;
        private RehashMethodBasedTable<int> _rehashBasedTable;
        private CombinedMethodBasedTable<int> _combinedMethodBasedTable;
        private int _capacity;
        private List<DataForAdd> _rehashMethodAddingData;
        private List<DataForAdd> _combinedMethodAddingData;
        private List<DataForSearch> _rehashMethodSearchingData;
        private List<DataForSearch> _combinedMethodSearchingData;
        private char[] _alphabet = new []{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                                         'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                                         's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        private (string key, int value)[] _testElements;
        private string[] _keysForSearch;
        static Form1() {
            Rnd = new Random();
        }

        public Form1() {
            InitializeComponent();
            _rehashMethodAddingData = new List<DataForAdd>();
            _combinedMethodAddingData = new List<DataForAdd>();
            _rehashMethodSearchingData = new List<DataForSearch>();
            _combinedMethodSearchingData = new List<DataForSearch>();
            dataGridView1.DataSource = _rehashMethodAddingData;
            dataGridView2.DataSource = _combinedMethodAddingData;
            dataGridView3.DataSource = _rehashMethodSearchingData;
            dataGridView4.DataSource = _combinedMethodSearchingData;
        }

        private (string key, int value)[] GetRandomElements(int capacity) {
            var uniqueSet = new HashSet<string>();
            (string, int)[] res = new(string, int)[capacity];
            for(var i = 0; i < capacity; ++i) {
                var keyLength = Rnd.Next(1, 40);
                var key = string.Empty;
                do {
                    key = GenerateKey();
                } while (uniqueSet.Contains(key));
                uniqueSet.Add(key);
                var value = Rnd.Next(0, 10000000);
                res[i] = (key, value);
            }
            return res;
        }
        
        private string GenerateKey() {
            var keyLength = Rnd.Next(1, 40);
            var key = string.Empty;
            for (var j = 0; j < keyLength; ++j) {
                key += _alphabet[Rnd.Next(_alphabet.Length)];
            }
            return key;
        }

        private string[] GetKeysForSearch(int capacity, (string key, int value)[] elements) {
            var res = new string[capacity];
            var containedElements = (int)(elements.Length * 0.7);
            for(var i = 0; i < containedElements; ++i) {
                res[i] = elements[i].key;
            }
            for(var i = containedElements; i < capacity; ++i) {
                res[i] = GenerateKey();
            }
            return res;
        }

        private async void TestAddBtn_Click(object sender, EventArgs e) {
            testSearchBtn.Enabled = false;
            _capacity = (int)numericUpDown1.Value;
            _rehashBasedTable = new RehashMethodBasedTable<int>(_capacity);
            _combinedMethodBasedTable = new CombinedMethodBasedTable<int>(_capacity);
            _testElements = GetRandomElements(_capacity);
            _keysForSearch = GetKeysForSearch(_capacity, _testElements);
            label1.Text = "Рехеширование. Добавление";
            label2.Text = "Комбинированный метод. Добавление";
            label3.Text = "Рехеширование. Поиск";
            label4.Text = "Комбинированный метод. Поиск";
            await Task.Run(() => {
                ClearDataSources();
                Invoke((MethodInvoker)(() => UpdateDataGrids()));
                foreach (var elem in _testElements) {
                    var rehashAddTime = _rehashBasedTable.Add(elem.key, elem.value);
                    var combinedAddTime = _combinedMethodBasedTable.Add(elem.key, elem.value);
                    var rehashAddData = new DataForAdd {
                        Key = elem.key,
                        Value = elem.value,
                        Time = rehashAddTime
                    };
                    var combinedAddData = new DataForAdd {
                        Key = elem.key,
                        Value = elem.value,
                        Time = combinedAddTime
                    };
                    _rehashMethodAddingData.Add(rehashAddData);
                    _combinedMethodAddingData.Add(combinedAddData);
                }
                var rehashTotalTime = _rehashMethodAddingData.Select(x => x.Time).Sum() / 1e9;
                var combinedTotalTime = _combinedMethodAddingData.Select(x => x.Time).Sum() / 1e9;
                Invoke((MethodInvoker)(() => label1.Text = $"Рехеширование. Добавление - {rehashTotalTime} c."));
                Invoke((MethodInvoker)(() => label2.Text = $"Комбинированный метод. Добавление - {combinedTotalTime} c."));
            });
            UpdateDataGrids();
            testSearchBtn.Enabled = true;
        }

        private async void TestSearchBtn_Click(object sender, EventArgs e) {
            await Task.Run(() => {
                foreach(var key in _keysForSearch) {
                    var rehashSearchRes = _rehashBasedTable.Search(key);
                    var combinedSearchRes = _combinedMethodBasedTable.Search(key);
                    var rehashSearchData = new DataForSearch {
                        Key = key,
                        Contais = rehashSearchRes.contains,
                        Value = rehashSearchRes.contains ? (int?)rehashSearchRes.value : null,
                        Time = rehashSearchRes.time
                    };
                    var combinedSearchData = new DataForSearch {
                        Key = key,
                        Contais = combinedSearchRes.contains,
                        Value = combinedSearchRes.contains ? (int?)combinedSearchRes.value : null,
                        Time = combinedSearchRes.time
                    };
                    _rehashMethodSearchingData.Add(rehashSearchData);
                    _combinedMethodSearchingData.Add(combinedSearchData);
                }
                var rehashTotalTime = _rehashMethodSearchingData.Select(x => x.Time).Sum() / 1e9;
                var combinedTotalTime = _combinedMethodSearchingData.Select(x => x.Time).Sum() / 1e9;
                Invoke((MethodInvoker)(() => label3.Text = $"Рехеширование. Поиск - {rehashTotalTime} c."));
                Invoke((MethodInvoker)(() => label4.Text = $"Комбинированный метод. Поиск - {combinedTotalTime} c."));
            });
            UpdateDataGrids();
            testSearchBtn.Enabled = false;
        }

        private void ClearDataSources() {
            _rehashMethodAddingData.Clear();
            _rehashMethodSearchingData.Clear();
            _combinedMethodAddingData.Clear();
            _combinedMethodSearchingData.Clear();
        }

        private void RefrashGrid(DataGridView dgv) {
            (dgv.BindingContext[dgv.DataSource] as CurrencyManager).Refresh();
        }

        private void UpdateDataGrids() {
            RefrashGrid(dataGridView1);
            RefrashGrid(dataGridView2);
            RefrashGrid(dataGridView3);
            RefrashGrid(dataGridView4);
        }
    }
}